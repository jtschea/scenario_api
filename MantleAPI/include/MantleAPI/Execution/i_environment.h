/*******************************************************************************
 * Copyright (c) 2021, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

//-----------------------------------------------------------------------------
/** @file  i_environment.h */
//-----------------------------------------------------------------------------

#ifndef MANTLEAPI_EXECUTION_IENVIRONMENT_H
#define MANTLEAPI_EXECUTION_IENVIRONMENT_H

#include <MantleAPI/Common/simulation_time.h>
#include <MantleAPI/EnvironmentalConditions/date_time.h>
#include <MantleAPI/EnvironmentalConditions/road_condition.h>
#include <MantleAPI/EnvironmentalConditions/weather.h>
#include <MantleAPI/Map/i_coord_converter.h>
#include <MantleAPI/Map/i_lane_location_query_service.h>
#include <MantleAPI/Traffic/i_entity_repository.h>

#include <string>

namespace mantle_api
{
class IEnvironment
{
  public:
    virtual ~IEnvironment() = default;

    /// Load a map file and parse it into the memory.
    ///
    /// @param file_path map file path from the scenario file. If this path is not resolved by the engine, the
    ///                  environment must do so.
    virtual void CreateMap(const std::string& map_file_path, const std::vector<Position>& map_region) = 0;

    /// Creates a controller from the given config. A created controller can be assigned to multiple entities
    ///
    /// @param config Specifies what kind of controller shall be created. The config needs to contain a controller ID
    ///               in order to be able to assign this controller to an entity.
    virtual void CreateController(std::unique_ptr<IControllerConfig> config) = 0;

    /// Assigns an entity to a copy of the specified controller. This controller needs to be created beforehand. Only
    /// one controller can be added to an entity
    ///
    /// @param entity The entity to be manipulated by the specified controller.
    /// @param controller_id Identifies the controller to manipulate the entity.
    virtual void AddEntityToController(IEntity& entity, UniqueId controller_id) = 0;

    virtual void RemoveControllerFromEntity(UniqueId entity_id) = 0;

    /// Updates the control strategies for an entity.
    ///
    /// @param entity_id          Specifies the entity to be updated
    /// @param control_strategies Specifies the desired movement behaviour for the entity
    virtual void UpdateControlStrategies(
        UniqueId entity_id, std::vector<std::unique_ptr<mantle_api::ControlStrategy>>& control_strategies) = 0;

    /// Checks, if a control strategy of a certain type for a specific entity has been fulfilled
    ///
    /// @param entity_id    The entity to check
    /// @param type         The control strategy type
    virtual bool HasControlStrategyGoalBeenReached(UniqueId entity_id, mantle_api::ControlStrategyType type) const = 0;

    virtual const ILaneLocationQueryService& GetQueryService() const = 0;
    virtual const ICoordConverter* GetConverter() const = 0;

    virtual IEntityRepository& GetEntityRepository() = 0;
    virtual const IEntityRepository& GetEntityRepository() const = 0;

    /// @brief DateTime in UTC (converted from RFC 3339 standard)
    virtual void SetDateTime(DateTime date_time) = 0;
    virtual DateTime GetDateTime() = 0;

    /// @brief Time since start of simulation and delta time to previous step
    virtual SimulationTime GetSimulationTime() = 0;

    virtual void SetWeather(Weather weather) = 0;
    virtual void SetRoadCondition(std::vector<FrictionPatch> friction_patches) = 0;
};
}  // namespace mantle_api

#endif  // MANTLEAPI_EXECUTION_IENVIRONMENT_H
