/*******************************************************************************
 * Copyright (c) 2021, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

//-----------------------------------------------------------------------------
/** @file  weather.h */
//-----------------------------------------------------------------------------

#ifndef MANTLEAPI_ENVIRONMENTALCONDITIONS_WEATHER_H
#define MANTLEAPI_ENVIRONMENTALCONDITIONS_WEATHER_H

#include <units.h>

namespace mantle_api
{

enum class Precipitation
{
    kUnknown,
    kOther,
    kNone,
    kVeryLight,
    kLight,
    kModerate,
    kHeavy,
    kVeryHeavy,
    kExtreme
};

enum class Fog
{
    kUnknown,
    kOther,
    kExcellentVisibility,
    kGoodVisibility,
    kModerateVisibility,
    kPoorVisibility,
    kMist,
    kLight,
    kThick,
    kDense
};

enum class Illumination
{
    kUnknown,
    kOther,
    kLevel1,
    kLevel2,
    kLevel3,
    kLevel4,
    kLevel5,
    kLevel6,
    kLevel7,
    kLevel8,
    kLevel9
};

struct Weather
{
    Fog fog{Fog::kExcellentVisibility};
    Precipitation precipitation{Precipitation::kNone};
    Illumination illumination{Illumination::kOther};
    units::concentration::percent_t humidity{0.0};
    units::temperature::kelvin_t temperature{0.0};
    units::pressure::pascal_t atmospheric_pressure{0.0};
};

}  // namespace mantle_api

#endif  // MANTLEAPI_ENVIRONMENTALCONDITIONS_WEATHER_H
