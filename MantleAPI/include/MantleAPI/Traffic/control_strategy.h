/*******************************************************************************
 * Copyright (c) 2021, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

//-----------------------------------------------------------------------------
/** @file  control_strategy.h */
//-----------------------------------------------------------------------------

#ifndef MANTLEAPI_TRAFFIC_CONTROLSTRATEGY_H
#define MANTLEAPI_TRAFFIC_CONTROLSTRATEGY_H

#include <MantleAPI/Common/spline.h>
#include <MantleAPI/Common/vector.h>
#include <MantleAPI/Common/trajectory.h>

#include <vector>

namespace mantle_api
{

enum class MovementDomain
{
    kUndefined = 0,
    kLateral,
    kLongitudinal,
    kBoth
};

enum class ControlStrategyType
{
    kUndefined = 0,
    kKeepVelocity,
    kKeepLaneOffset,
    kFollowHeadingSpline,
    kFollowLateralOffsetSpline,
    kFollowVelocitySpline,
    kFollowRoute,
    kAcquireLaneOffset,
    kFollowTrajectory
};

struct ControlStrategy
{
    virtual ~ControlStrategy() = default;

    // TODO: extend by bool use_dynamic_constraints when needed (false assumed at the moment)

    MovementDomain movement_domain{MovementDomain::kUndefined};
    ControlStrategyType type{ControlStrategyType::kUndefined};
};

inline bool operator==(const ControlStrategy& lhs, const ControlStrategy& rhs) noexcept
{
    return lhs.movement_domain == rhs.movement_domain && lhs.type == rhs.type;
}

inline bool operator!=(const ControlStrategy& lhs, const ControlStrategy& rhs) noexcept
{
    return !(lhs == rhs);
}

struct KeepVelocityControlStrategy : public ControlStrategy
{
    KeepVelocityControlStrategy()
    {
        movement_domain = MovementDomain::kLongitudinal;
        type = ControlStrategyType::kKeepVelocity;
    }
    // Doesn't need configuration attributes. Controller keeps current velocity on adding entity or update
};

struct KeepLaneOffsetControlStrategy : public ControlStrategy
{
    KeepLaneOffsetControlStrategy()
    {
        movement_domain = MovementDomain::kLateral;
        type = ControlStrategyType::kKeepLaneOffset;
    }
    // Doesn't need configuration attributes. Controller keeps current lane offset on adding entity or update
};

struct FollowHeadingSplineControlStrategy : public ControlStrategy
{
    FollowHeadingSplineControlStrategy()
    {
        movement_domain = MovementDomain::kLateral;
        type = ControlStrategyType::kFollowHeadingSpline;
    }

    std::vector<mantle_api::SplineSection<units::angle::radian_t>> heading_splines;
    double default_value{0};
};

struct FollowVelocitySplineControlStrategy : public ControlStrategy
{
    FollowVelocitySplineControlStrategy()
    {
        movement_domain = MovementDomain::kLongitudinal;
        type = ControlStrategyType::kFollowVelocitySpline;
    }

    std::vector<mantle_api::SplineSection<units::velocity::meters_per_second_t>> velocity_splines;
    double default_value{0};
};

inline bool operator==(const FollowVelocitySplineControlStrategy& lhs,
                       const FollowVelocitySplineControlStrategy& rhs) noexcept
{
    return lhs.default_value == rhs.default_value && lhs.velocity_splines == rhs.velocity_splines;
}

inline bool operator!=(const FollowVelocitySplineControlStrategy& lhs,
                       const FollowVelocitySplineControlStrategy& rhs) noexcept
{
    return !(lhs == rhs);
}

struct FollowLateralOffsetSplineControlStrategy : public ControlStrategy
{
    FollowLateralOffsetSplineControlStrategy()
    {
        movement_domain = MovementDomain::kLateral;
        type = ControlStrategyType::kFollowLateralOffsetSpline;
    }

    std::vector<mantle_api::SplineSection<units::length::meter_t>> lateral_offset_splines;
};

struct FollowRouteControlStrategy : public ControlStrategy
{
    FollowRouteControlStrategy()
    {
        movement_domain = MovementDomain::kLateral;
        type = ControlStrategyType::kFollowRoute;
    }

    std::vector<mantle_api::Vec3<units::length::meter_t>> waypoints;
};

enum class Dimension
{
    kUndefined = 0,
    kDistance,
    kRate,
    kTime
};

enum class Shape
{
    kUndefined = 0,
    kStep,
    kCubic,
    kLinear,
    kSinusoidal
};

struct TransitionDynamics
{
    Dimension dimension{Dimension::kUndefined};
    Shape shape{Shape::kUndefined};
    double value{0.0};
};

struct AcquireLaneOffsetControlStrategy : public ControlStrategy
{
    AcquireLaneOffsetControlStrategy()
    {
        movement_domain = MovementDomain::kLateral;
        type = ControlStrategyType::kAcquireLaneOffset;
    }

    int road_id{};
    int lane_id{};
    units::length::meter_t offset{};
    TransitionDynamics transition_dynamics;
};

enum class ReferenceContext
{
    kAbsolute = 0,
    kRelative
};

struct FollowTrajectoryControlStrategy : public ControlStrategy
{
    // TODO: Extend the FollowTrajectoryControlStrategy to support shapes like NURBS and clothoid

    struct TrajectoryTimeReference
    {
        ReferenceContext domainAbsoluteRelative;
        double scale{0.0};
        double offset{0.0};
    };
    
    FollowTrajectoryControlStrategy()
    {
        movement_domain = MovementDomain::kBoth;
        type = ControlStrategyType::kFollowTrajectory;
    }

    Trajectory trajectory;
    std::optional<TrajectoryTimeReference> timeReference;
};

}  // namespace mantle_api

#endif  // MANTLEAPI_TRAFFIC_CONTROLSTRATEGY_H
