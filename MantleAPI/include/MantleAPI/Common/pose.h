/*******************************************************************************
 * Copyright (c) 2021, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

//-----------------------------------------------------------------------------
/** @file  pose.h */
//-----------------------------------------------------------------------------

#ifndef MANTLEAPI_COMMON_POSE_H
#define MANTLEAPI_COMMON_POSE_H

#include <MantleAPI/Common/orientation.h>
#include <MantleAPI/Common/vector.h>

#include <units.h>

#include <iostream>


namespace mantle_api
{

struct Pose
{
    Vec3<units::length::meter_t> position{};
    Orientation3<units::angle::radian_t> orientation{};

    bool operator== (const Pose& other) const
    {
        return other.position == position
                && other.orientation == orientation;
    }

    friend inline std::ostream& operator<<(std::ostream& os, const Pose& pose);
};

std::ostream& operator<<(std::ostream& os, const Pose& pose)
{
    os << "position (" 
        << pose.position.x
        << ", " << pose.position.y
        << ", " << pose.position.z
        << "), orientation (" << pose.orientation.yaw
        << ", " << pose.orientation.pitch
        << ", " << pose.orientation.roll
        << ")";

    return os;
}

}  // namespace mantle_api

#endif  // MANTLEAPI_COMMON_POSE_H
