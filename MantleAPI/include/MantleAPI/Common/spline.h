
/*******************************************************************************
 * Copyright (c) 2021, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

//-----------------------------------------------------------------------------
/** @file  spline.h */
//-----------------------------------------------------------------------------

#ifndef MANTLEAPI_COMMON_SPLINE_H
#define MANTLEAPI_COMMON_SPLINE_H

#include <MantleAPI/Common/floating_point_helper.h>
#include <MantleAPI/Common/time_utils.h>
#include <MantleAPI/Common/vector.h>
#include <units.h>

#include <array>

namespace mantle_api
{

template <typename T>
struct SplineSegment
{
    Vec3<T> a;
    Vec3<T> b;
    Vec3<T> c;
    Vec3<T> d;
};

template <typename T, class = typename std::enable_if_t<units::traits::is_unit_t<T>::value>>
struct SplineSection
{
    Time start_time{0};
    Time end_time{0};
    /// @brief Represents the polynomial.
    ///
    /// The array stores in format \f$[a_3, a_2, a_1, a_0]\f$ for a polynomial in form
    /// \f[
    ///   P(x) = \sum_{i=0}^{3} a_{i} x^{i} = a_3 x^3 + a_2 x^2 + a_1 x + a_0
    /// \f]
    std::array<T, 4> polynomial{0, 0, 0, 0};
};

/// @brief Equality comparison for SplineSection.
template <typename T, class = typename std::enable_if_t<units::traits::is_unit_t<T>::value>>
inline bool operator==(const SplineSection<T>& lhs, const SplineSection<T>& rhs) noexcept
{
    return lhs.start_time == rhs.start_time && lhs.end_time == rhs.end_time &&
           std::equal(lhs.polynomial.begin(),
                      lhs.polynomial.end(),
                      rhs.polynomial.begin(),
                      [](const T& a, const T& b) { return IsEqual(a, b); });
}

}  // namespace mantle_api

#endif  // MANTLEAPI_COMMON_SPLINE_H
