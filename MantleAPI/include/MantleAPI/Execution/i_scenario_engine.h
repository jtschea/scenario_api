/*******************************************************************************
 * Copyright (c) 2021, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

//-----------------------------------------------------------------------------
/** @file  i_scenario_engine.h */
//-----------------------------------------------------------------------------

#ifndef MANTLEAPI_EXECUTION_ISCENARIOENGINE_H
#define MANTLEAPI_EXECUTION_ISCENARIOENGINE_H

#include <MantleAPI/Execution/scenario_info.h>

#include <string>

namespace mantle_api
{
class IScenarioEngine
{
  public:
    virtual ~IScenarioEngine() = default;

    /// Initialization of the scenario state, e.g. loading the scenario file, map file, etc.
    virtual void Init() = 0;

    /// Provide information about the scenario loaded in `Init()`
    virtual ScenarioInfo GetScenarioInfo() const = 0;

    /// Calculate the new state of the scenario implementation.
    ///
    /// Calling this function after `IsFinished()` should be a no-op.
    /// @see IsFinished()
    virtual void Step() = 0;

    /// Indicates whether the scenario implementation has finished processing the scenario (end of scenario is reached).
    /// @return `true` if processing the scenario is complete, `false` otherwise.
    virtual bool IsFinished() const = 0;

    virtual void ActivateExternalHostControl() = 0;
};
}  // namespace mantle_api

#endif  // MANTLEAPI_EXECUTION_ISCENARIOENGINE_H
