/*******************************************************************************
 * Copyright (c) 2021, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

//-----------------------------------------------------------------------------
/** @file  entity_properties.h */
//-----------------------------------------------------------------------------

#ifndef MANTLEAPI_TRAFFIC_ENTITYPROPERTIES_H
#define MANTLEAPI_TRAFFIC_ENTITYPROPERTIES_H

#include <MantleAPI/Common/bounding_box.h>
#include <MantleAPI/Common/floating_point_helper.h>
#include <MantleAPI/Common/vector.h>

#include <string>
#include <map>

namespace mantle_api
{

enum class EntityType
{
    // Other (unspecified but known) type of moving object.
    kOther = 1,
    // Object is a vehicle.
    kVehicle = 2,
    // Object is a pedestrian.
    kPedestrian = 3,
    // Object is an animal.
    kAnimal = 4
};

/// Basic properties that describe scenario entities.
struct EntityProperties
{
    virtual ~EntityProperties() = default;

    BoundingBox bounding_box{};
    EntityType type{EntityType::kOther};
    std::string model{};
    std::map<std::string, std::string> properties{};
};

inline bool operator==(const EntityProperties& lhs, const EntityProperties& rhs) noexcept
{
    return lhs.bounding_box == rhs.bounding_box && lhs.type == rhs.type && lhs.model == rhs.model;
}

enum class VehicleClass
{
    kOther = 1,  // Other (unspecified but known) type of vehicle.
    // Vehicle is a small car.
    // Definition: Hatchback car with maximum length 4 m.
    kSmall_car = 2,
    // Vehicle is a compact car.
    // Definition: Hatchback car with length between 4 and 4.5 m.
    kCompact_car = 3,
    // Vehicle is a medium car.
    // Definition: Hatchback or sedan with length between 4.5 and 5 m.
    kMedium_car = 4,
    // Vehicle is a luxury car.
    // Definition: Sedan or coupe that is longer then 5 m.
    kLuxury_car = 5,
    // Vehicle is a delivery van.
    // Definition: A delivery van.
    kDelivery_van = 6,
    // Vehicle is a heavy truck.
    kHeavy_truck = 7,
    // Vehicle is a truck with semitrailer.
    kSemitrailer = 8,
    // Vehicle is a trailer (possibly attached to another vehicle).
    kTrailer = 9,
    // Vehicle is a motorbike or moped.
    kMotorbike = 10,
    // Vehicle is a bicycle (without motor and specific lights).
    kBicycle = 11,
    // Vehicle is a bus.
    kBus = 12,
    // Vehicle is a tram.
    kTram = 13,
    // Vehicle is a train.
    kTrain = 14,
    // Vehicle is a wheelchair.
    kWheelchair = 15,
    // Vehicle type not specified properly.
    kInvalid = -1
};

enum class IndicatorState
{
    // Indicator state is unknown (must not be used in ground truth).
    kUnknown = 0,
    // Other (unspecified but known) state of indicator.
    kOther = 1,
    // Indicators are off.
    kOff = 2,
    // Left indicator is on.
    kLeft = 3,
    // Right indicator is on.
    kRight = 4,
    // Hazard/warning light, i.e. both indicators, are on.
    kWarning = 5
};

enum class ExternalControlState
{
    kOff = 0,
    kFull = 1,
    kLateralOnly = 2,
    kLongitudinalOnly = 3
};

struct Performance
{
    units::velocity::meters_per_second_t max_speed{0.0};
    units::acceleration::meters_per_second_squared_t max_acceleration{0.0};
    units::acceleration::meters_per_second_squared_t max_deceleration{0.0};
};

inline bool operator==(const Performance& lhs, const Performance& rhs) noexcept
{
    return IsEqual(lhs.max_speed, rhs.max_speed) &&
            IsEqual(lhs.max_acceleration, rhs.max_acceleration) &&
            IsEqual(lhs.max_deceleration, rhs.max_deceleration);
}

struct Axle
{
    units::angle::radian_t max_steering{0.0};
    units::length::meter_t wheel_diameter{0.0};
    units::length::meter_t track_width{0.0};
    Vec3<units::length::meter_t> bb_center_to_axle_center{};
};

inline bool operator==(const Axle& lhs, const Axle& rhs) noexcept
{
    return IsEqual(lhs.max_steering, rhs.max_steering) &&
            IsEqual(lhs.wheel_diameter, rhs.wheel_diameter) &&
            IsEqual(lhs.track_width, rhs.track_width) &&
            lhs.bb_center_to_axle_center == rhs.bb_center_to_axle_center;
}

struct VehicleProperties : public EntityProperties
{
    VehicleClass classification{VehicleClass::kOther};

    Performance performance{};

    Axle front_axle{};
    Axle rear_axle{};

    bool is_host{false};
    // TODO: remove, once external control for traffic is implemented through controllers
    bool is_controlled_externally{false};
};

inline bool operator==(const VehicleProperties& lhs, const VehicleProperties& rhs) noexcept
{
    return lhs.bounding_box == rhs.bounding_box && lhs.type == rhs.type && lhs.model == rhs.model &&
           lhs.classification == rhs.classification && lhs.performance == rhs.performance &&
           lhs.front_axle == rhs.front_axle && lhs.rear_axle == rhs.rear_axle &&
           lhs.is_host == rhs.is_host;
}

struct PedestrianProperties : public EntityProperties
{
};

struct StaticObjectProperties : public EntityProperties
{
};

}  // namespace mantle_api

#endif  // MANTLEAPI_TRAFFIC_ENTITYPROPERTIES_H
