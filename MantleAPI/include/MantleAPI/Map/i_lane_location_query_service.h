/*******************************************************************************
 * Copyright (c) 2021, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

//-----------------------------------------------------------------------------
/** @file  i_lane_location_query_service.h */
//-----------------------------------------------------------------------------

#ifndef MANTLEAPI_MAP_ILANELOCATIONQUERYSERVICE_H
#define MANTLEAPI_MAP_ILANELOCATIONQUERYSERVICE_H

#include <MantleAPI/Common/i_identifiable.h>
#include <MantleAPI/Common/orientation.h>
#include <MantleAPI/Common/vector.h>
#include <units.h>

namespace mantle_api
{

/// Abstraction layer for all map related functions.
class ILaneLocationQueryService
{
  public:
    virtual ~ILaneLocationQueryService() = default;

    /// TODO: Currently LaneLocationProvider just inherits from this for the sake of easily passing it to the
    /// controllers
    /// Therefore the GetMapObjectById function has been removed from the interface - for now.
    /// We need to think about proper interface functions we want to define here (GetLaneLocation? GetLanes? But this
    /// would add a rat-tail of more interfaces we need to define (ILaneLocation, ILane, ...?)
    // virtual const IIdentifiable& GetMapObjectById(UniqueId id) = 0;

    virtual Orientation3<units::angle::radian_t> GetLaneOrientation(
        const Vec3<units::length::meter_t>& position) const = 0;
    virtual Vec3<units::length::meter_t> GetUpwardsShiftedLanePosition(const Vec3<units::length::meter_t>& position,
                                                                       double upwards_shift,
                                                                       bool allow_invalid_positions = false) const = 0;
    virtual bool IsPositionOnLane(const Vec3<units::length::meter_t>& position) const = 0;
};

}  // namespace mantle_api

#endif  // MANTLEAPI_MAP_ILANELOCATIONQUERYSERVICE_H
