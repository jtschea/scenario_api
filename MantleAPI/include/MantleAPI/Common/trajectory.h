/*******************************************************************************
 * Copyright (c) 2021, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

//-----------------------------------------------------------------------------
/** @file  trajectory.h */
//-----------------------------------------------------------------------------

#ifndef MANTLEAPI_COMMON_TRAJECTORY_H
#define MANTLEAPI_COMMON_TRAJECTORY_H

#include <string>
#include <variant>

#include <MantleAPI/Common/poly_line.h>

namespace mantle_api
{
struct Trajectory
{
    std::string name;
    std::variant<PolyLine> type;

    friend std::ostream& operator<<(std::ostream& os, const Trajectory& trajectory);
};

inline std::ostream& operator<<(std::ostream& os, const Trajectory& trajectory)
{
    os << "Trajectory \"" << trajectory.name;
    
    if(std::holds_alternative<PolyLine>(trajectory.type))
    {
        const auto &polyLine = std::get<PolyLine>(trajectory.type);
        for (const auto &polyLinePoint : polyLine)
        {
            os << polyLinePoint;
        }
    }
    
    os << "\"\n";

    return os;
}

} // namespace mantle_api

#endif // MANTLEAPI_COMMON_TRAJECTORY_H