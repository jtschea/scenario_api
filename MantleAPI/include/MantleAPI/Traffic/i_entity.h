/*******************************************************************************
 * Copyright (c) 2021, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

//-----------------------------------------------------------------------------
/** @file  i_entity.h */
//-----------------------------------------------------------------------------

#ifndef MANTLEAPI_TRAFFIC_IENTITY_H
#define MANTLEAPI_TRAFFIC_IENTITY_H

#include <MantleAPI/Common/bounding_box.h>
#include <MantleAPI/Common/i_identifiable.h>
#include <MantleAPI/Common/pose.h>
#include <MantleAPI/Traffic/entity_properties.h>
#include <MantleAPI/Traffic/i_controller_config.h>

#include <memory>
#include <vector>

namespace mantle_api
{

/// Base interface for all static and dynamic scenario entities.
class IEntity : public IIdentifiable
{
  public:
    virtual void SetPosition(const Vec3<units::length::meter_t>& inert_pos) = 0;
    virtual Vec3<units::length::meter_t> GetPosition() const = 0;

    virtual void SetVelocity(const Vec3<units::velocity::meters_per_second_t>& velocity) = 0;
    virtual Vec3<units::velocity::meters_per_second_t> GetVelocity() const = 0;

    virtual void SetAcceleration(const Vec3<units::acceleration::meters_per_second_squared_t>& acceleration) = 0;
    virtual Vec3<units::acceleration::meters_per_second_squared_t> GetAcceleration() const = 0;

    virtual void SetOrientation(const Orientation3<units::angle::radian_t>& orientation) = 0;
    virtual Orientation3<units::angle::radian_t> GetOrientation() const = 0;

    virtual void SetOrientationRate(
        const Orientation3<units::angular_velocity::radians_per_second_t>& orientation_rate) = 0;
    virtual Orientation3<units::angular_velocity::radians_per_second_t> GetOrientationRate() const = 0;

    virtual void SetOrientationAcceleration(
        const Orientation3<units::angular_acceleration::radians_per_second_squared_t>& orientation_acceleration) = 0;
    virtual Orientation3<units::angular_acceleration::radians_per_second_squared_t> GetOrientationAcceleration()
        const = 0;

    virtual void SetProperties(std::unique_ptr<mantle_api::EntityProperties> properties) = 0;
    virtual EntityProperties* GetProperties() const = 0;

    virtual void SetAssignedLaneIds(const std::vector<std::uint64_t>& assigned_lane_ids) = 0;
    virtual std::vector<std::uint64_t> GetAssignedLaneIds() const = 0;
};

class IVehicle : public virtual IEntity
{
  public:
    virtual VehicleProperties* GetProperties() const = 0;

    virtual void SetIndicatorState(IndicatorState state) = 0;
    virtual IndicatorState GetIndicatorState() const = 0;

    //    virtual bool IsHost() const = 0;
    //    virtual void SetHost() = 0;
};

class IPedestrian : public virtual IEntity
{
  public:
    virtual PedestrianProperties* GetProperties() const = 0;
};

class IStaticObject : public virtual IEntity
{
  public:
    virtual StaticObjectProperties* GetProperties() const = 0;
};

}  // namespace mantle_api

#endif  // MANTLEAPI_TRAFFIC_IENTITY_H
