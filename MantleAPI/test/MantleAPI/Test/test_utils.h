/*******************************************************************************
 * Copyright (c) 2021, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

//-----------------------------------------------------------------------------
/** @file  test_utils.h */
//-----------------------------------------------------------------------------

#pragma once

#include <MantleAPI/Common/i_identifiable.h>
#include <MantleAPI/Common/position.h>
#include <MantleAPI/EnvironmentalConditions/road_condition.h>
#include <MantleAPI/EnvironmentalConditions/weather.h>
#include <MantleAPI/Execution/i_environment.h>
#include <MantleAPI/Map/i_coord_converter.h>
#include <MantleAPI/Map/i_lane_location_query_service.h>
#include <MantleAPI/Traffic/entity_properties.h>
#include <MantleAPI/Traffic/i_controller_config.h>
#include <MantleAPI/Traffic/i_entity.h>
#include <MantleAPI/Traffic/i_entity_repository.h>
#include <gmock/gmock.h>
#include <gtest/gtest.h>

namespace mantle_api
{

class MockConverter : public mantle_api::ICoordConverter
{
  public:
    MOCK_METHOD(mantle_api::Vec3d, Convert, (mantle_api::Position position), (const override));

    mantle_api::Position Convert(mantle_api::Vec3d vec) const override
    {
        std::ignore = vec;
        return mantle_api::Position{};
    }
};

class MockVehicle : public mantle_api::IVehicle
{
  public:
    MOCK_METHOD(mantle_api::UniqueId, GetUniqueId, (), (const, override));

    void SetName(const std::string& name) override { name_ = name; }
    const std::string& GetName() const override { return name_; }

    MOCK_METHOD(void, SetPosition, (const mantle_api::Vec3d& inert_pos), (override));
    MOCK_METHOD(mantle_api::Vec3d, GetPosition, (), (const, override));

    MOCK_METHOD(void, SetVelocity, (const mantle_api::Vec3d& velocity), (override));
    MOCK_METHOD(mantle_api::Vec3d, GetVelocity, (), (const, override));

    MOCK_METHOD(void, SetAcceleration, (const mantle_api::Vec3d& acceleration), (override));
    MOCK_METHOD(mantle_api::Vec3d, GetAcceleration, (), (const, override));

    MOCK_METHOD(void, SetOrientation, (const mantle_api::Orientation3d& orientation), (override));
    MOCK_METHOD(mantle_api::Orientation3d, GetOrientation, (), (const, override));

    MOCK_METHOD(void, SetOrientationRate, (const mantle_api::Orientation3d& orientation_rate), (override));
    MOCK_METHOD(mantle_api::Orientation3d, GetOrientationRate, (), (const, override));

    MOCK_METHOD(void,
                SetOrientationAcceleration,
                (const mantle_api::Orientation3d& orientation_acceleration),
                (override));
    MOCK_METHOD(mantle_api::Orientation3d, GetOrientationAcceleration, (), (const, override));

    MOCK_METHOD(void, SetAssignedLaneIds, (const std::vector<std::uint64_t>& ids), (override));
    MOCK_METHOD(std::vector<std::uint64_t>, GetAssignedLaneIds, (), (const, override));

    void SetProperties(std::unique_ptr<mantle_api::EntityProperties> properties) override { std::ignore = properties; }
    mantle_api::VehicleProperties* GetProperties() const override
    {
        return static_cast<mantle_api::VehicleProperties*>(properties_.get());
    }

    void SetIndicatorState(mantle_api::IndicatorState state) override { std::ignore = state; }
    mantle_api::IndicatorState GetIndicatorState() const override { return mantle_api::IndicatorState::kUnknown; }

  private:
    std::string name_{};
    std::unique_ptr<mantle_api::EntityProperties> properties_{nullptr};
};

class MockQueryService : public mantle_api::ILaneLocationQueryService
{
  public:
    Orientation3d GetLaneOrientation(const Vec3d& position) const override
    {
        std::ignore = position;
        return {};
    }

    Vec3d GetUpwardsShiftedLanePosition(const Vec3d& position,
                                        double upwards_shift,
                                        bool allowed_to_leave_lane = false) const override
    {
        std::ignore = position;
        std::ignore = upwards_shift;
        std::ignore = allowed_to_leave_lane;
        return mantle_api::Vec3d();
    }
    bool IsPositionOnLane(const Vec3d& position) const override
    {
        std::ignore = position;
        return false;
    }

  private:
    MockVehicle test_vehicle_{};
};

class MockPedestrian : public mantle_api::IPedestrian
{
  public:
    mantle_api::UniqueId GetUniqueId() const override { return 0; }

    void SetName(const std::string& name) override { name_ = name; }
    const std::string& GetName() const override { return name_; }

    MOCK_METHOD(void, SetPosition, (const mantle_api::Vec3d& inert_pos), (override));
    MOCK_METHOD(mantle_api::Vec3d, GetPosition, (), (const, override));

    MOCK_METHOD(void, SetVelocity, (const mantle_api::Vec3d& velocity), (override));
    MOCK_METHOD(mantle_api::Vec3d, GetVelocity, (), (const, override));

    MOCK_METHOD(void, SetAcceleration, (const mantle_api::Vec3d& acceleration), (override));
    MOCK_METHOD(mantle_api::Vec3d, GetAcceleration, (), (const, override));

    MOCK_METHOD(void, SetOrientation, (const mantle_api::Orientation3d& orientation), (override));
    MOCK_METHOD(mantle_api::Orientation3d, GetOrientation, (), (const, override));

    MOCK_METHOD(void, SetOrientationRate, (const mantle_api::Orientation3d& orientation_rate), (override));
    MOCK_METHOD(mantle_api::Orientation3d, GetOrientationRate, (), (const, override));

    MOCK_METHOD(void,
                SetOrientationAcceleration,
                (const mantle_api::Orientation3d& orientation_acceleration),
                (override));
    MOCK_METHOD(mantle_api::Orientation3d, GetOrientationAcceleration, (), (const, override));

    MOCK_METHOD(void, SetAssignedLaneIds, (const std::vector<std::uint64_t>& ids), (override));
    MOCK_METHOD(std::vector<std::uint64_t>, GetAssignedLaneIds, (), (const, override));

    void SetProperties(std::unique_ptr<mantle_api::EntityProperties> properties) override { std::ignore = properties; }
    mantle_api::PedestrianProperties* GetProperties() const override
    {
        return static_cast<mantle_api::PedestrianProperties*>(properties_.get());
    }

  private:
    std::string name_{};
    std::unique_ptr<mantle_api::EntityProperties> properties_{nullptr};
};

class MockStaticObject : public mantle_api::IStaticObject
{
  public:
    mantle_api::UniqueId GetUniqueId() const override { return 0; }

    void SetName(const std::string& name) override { name_ = name; }
    const std::string& GetName() const override { return name_; }

    MOCK_METHOD(void, SetPosition, (const mantle_api::Vec3d& inert_pos), (override));
    MOCK_METHOD(mantle_api::Vec3d, GetPosition, (), (const, override));

    MOCK_METHOD(void, SetVelocity, (const mantle_api::Vec3d& velocity), (override));
    MOCK_METHOD(mantle_api::Vec3d, GetVelocity, (), (const, override));

    MOCK_METHOD(void, SetAcceleration, (const mantle_api::Vec3d& acceleration), (override));
    MOCK_METHOD(mantle_api::Vec3d, GetAcceleration, (), (const, override));

    MOCK_METHOD(void, SetOrientation, (const mantle_api::Orientation3d& orientation), (override));
    MOCK_METHOD(mantle_api::Orientation3d, GetOrientation, (), (const, override));

    MOCK_METHOD(void, SetOrientationRate, (const mantle_api::Orientation3d& orientation_rate), (override));
    MOCK_METHOD(mantle_api::Orientation3d, GetOrientationRate, (), (const, override));

    MOCK_METHOD(void,
                SetOrientationAcceleration,
                (const mantle_api::Orientation3d& orientation_acceleration),
                (override));
    MOCK_METHOD(mantle_api::Orientation3d, GetOrientationAcceleration, (), (const, override));

    MOCK_METHOD(void, SetAssignedLaneIds, (const std::vector<std::uint64_t>& ids), (override));
    MOCK_METHOD(std::vector<std::uint64_t>, GetAssignedLaneIds, (), (const, override));

    void SetProperties(std::unique_ptr<mantle_api::EntityProperties> properties) override { std::ignore = properties; }
    mantle_api::StaticObjectProperties* GetProperties() const override
    {
        return static_cast<mantle_api::StaticObjectProperties*>(properties_.get());
    }

  private:
    std::string name_{};
    std::unique_ptr<mantle_api::EntityProperties> properties_{nullptr};
};

class MockEntityRepository : public mantle_api::IEntityRepository
{
  public:
    MOCK_METHOD(mantle_api::IVehicle&,
                Create,
                (const std::string& name, const mantle_api::VehicleProperties& properties),
                (override));

    mantle_api::IVehicle& Create(mantle_api::UniqueId id,
                                 const std::string& name,
                                 const mantle_api::VehicleProperties& properties) override
    {
        std::ignore = id;
        std::ignore = name;
        std::ignore = properties;
        return test_vehicle_;
    }

    MOCK_METHOD(mantle_api::IPedestrian&,
                Create,
                (const std::string& name, const mantle_api::PedestrianProperties& properties),
                (override));

    mantle_api::IPedestrian& Create(mantle_api::UniqueId id,
                                    const std::string& name,
                                    const mantle_api::PedestrianProperties& properties) override
    {
        std::ignore = id;
        std::ignore = name;
        std::ignore = properties;
        return test_pedestrian_;
    }

    MOCK_METHOD(mantle_api::IStaticObject&,
                Create,
                (const std::string& name, const mantle_api::StaticObjectProperties& properties),
                (override));

    mantle_api::IStaticObject& Create(mantle_api::UniqueId id,
                                      const std::string& name,
                                      const mantle_api::StaticObjectProperties& properties) override
    {
        std::ignore = id;
        std::ignore = name;
        std::ignore = properties;
        return test_static_object_;
    }

    mantle_api::IEntity& Get(const std::string& name) override
    {
        std::ignore = name;
        return test_vehicle_;
    }

    mantle_api::IEntity& Get(mantle_api::UniqueId id) override
    {
        std::ignore = id;
        return test_vehicle_;
    }

    mantle_api::IVehicle& GetHost() override { return test_vehicle_; }

    const std::vector<std::unique_ptr<mantle_api::IEntity>>& GetEntities() const override { return entities_; }

    void Delete(const std::string& name) override { std::ignore = name; }
    bool Contains(UniqueId id) const override
    {
        std::ignore = id;
        return false;
    }
    void Delete(UniqueId id) override { std::ignore = id; }

    //    const std::vector<mantle_api::IEntity>& GetEntities() const override { return <#initializer #>{}; }
    //    std::vector<mantle_api::IEntity>& GetEntities() override { return <#initializer #>; }

  private:
    MockVehicle test_vehicle_{};
    MockPedestrian test_pedestrian_{};
    MockStaticObject test_static_object_{};
    std::vector<std::unique_ptr<mantle_api::IEntity>> entities_{};
};

class MockEnvironment : public mantle_api::IEnvironment
{
  public:
    MOCK_METHOD(void,
                CreateMap,
                (const std::string& file_path, const std::vector<mantle_api::Position>& map_region),
                (override)

    );

    MOCK_METHOD(void, CreateController, (std::unique_ptr<IControllerConfig> config), (override)

    );

    MOCK_METHOD(void, AddEntityToController, (mantle_api::IEntity & entity, std::uint64_t controller_id), (override)

    );

    MOCK_METHOD(void, RemoveControllerFromEntity, (std::uint64_t entity_id), (override));

    MOCK_METHOD(void,
                UpdateControlStrategies,
                (std::uint64_t entity_id,
                 std::vector<std::unique_ptr<mantle_api::ControlStrategy>>& control_strategies),
                (override));

    MOCK_METHOD(bool,
                HasControlStrategyGoalBeenReached,
                (std::uint64_t entity_id, mantle_api::ControlStrategyType type),
                (const, override));

    const mantle_api::ILaneLocationQueryService& GetQueryService() const override { return query_service_; }

    const mantle_api::ICoordConverter* GetConverter() const override { return &converter_; }

    mantle_api::IEntityRepository& GetEntityRepository() override { return entity_repository_; }

    const mantle_api::IEntityRepository& GetEntityRepository() const override { return entity_repository_; }

    void SetWeather(mantle_api::Weather weather) override { std::ignore = weather; }

    void SetRoadCondition(std::vector<mantle_api::FrictionPatch> friction_patches) override
    {
        std::ignore = friction_patches;
    }

    void SetDateTime(mantle_api::DateTime date_time) override { std::ignore = date_time; }

    mantle_api::DateTime GetDateTime() override { return mantle_api::DateTime(); }

    MOCK_METHOD(mantle_api::SimulationTime, GetSimulationTime, (), (override));

  private:
    MockQueryService query_service_{};
    MockEntityRepository entity_repository_{};
    MockConverter converter_{};
};

}  // namespace mantle_api
