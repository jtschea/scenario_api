/*******************************************************************************
 * Copyright (c) 2021, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

//-----------------------------------------------------------------------------
/** @file  date_time.h */
//-----------------------------------------------------------------------------

#ifndef MANTLEAPI_ENVIRONMENTALCONDITIONS_DATETIME_H
#define MANTLEAPI_ENVIRONMENTALCONDITIONS_DATETIME_H

#include <MantleAPI/Common/time_utils.h>

namespace mantle_api
{

// TODO: Delete this struct and use Time directly in Get/SetDateTime once the move to the MantleAPI is complete
struct [[deprecated]] DateTime
{
    Time date_time{0};
};

}  // namespace mantle_api
#endif  // MANTLEAPI_ENVIRONMENTALCONDITIONS_DATETIME_H
