/*******************************************************************************
 * Copyright (c) 2021, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

//-----------------------------------------------------------------------------
/** @file  entity_helper.h */
//-----------------------------------------------------------------------------

#ifndef MANTLEAPI_TRAFFIC_ENTITY_HELPER_H
#define MANTLEAPI_TRAFFIC_ENTITY_HELPER_H

#include <MantleAPI/Traffic/i_entity.h>

#include <cmath>

namespace mantle_api
{

void SetSpeed(mantle_api::IEntity* entity, double velocity)
{
    auto orientation = entity->GetOrientation();

    double cos_elevation = std::cos(orientation.pitch);
    mantle_api::Vec3d velocity_vector{velocity * std::cos(orientation.yaw) * cos_elevation,
                                      velocity * std::sin(orientation.yaw) * cos_elevation,
                                      velocity * -std::sin(orientation.pitch)};

    entity->SetVelocity(velocity_vector);
}

}  // namespace mantle_api

#endif  // MANTLEAPI_TRAFFIC_ENTITY_HELPER_H
