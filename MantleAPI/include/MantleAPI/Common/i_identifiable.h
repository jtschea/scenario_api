/*******************************************************************************
 * Copyright (c) 2021, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

//-----------------------------------------------------------------------------
/** @file  i_identifiable.h */
//-----------------------------------------------------------------------------

#ifndef MANTLEAPI_COMMON_IIDENTIFIABLE_H
#define MANTLEAPI_COMMON_IIDENTIFIABLE_H

#include <cstdint>
#include <string>

namespace mantle_api
{
using UniqueId = std::uint64_t;
constexpr UniqueId InvalidId{std::numeric_limits<UniqueId>::max()};

/// Common interface for all classes that can be referenced by an ID or name.
class IIdentifiable
{
  public:
    virtual ~IIdentifiable() = default;

    /// The unique id is provided and maintained by the scenario simulator.
    virtual UniqueId GetUniqueId() const = 0;
    /// Scenario specific name of an object.
    ///
    /// The scenario description is responsible for keeping the name unique.
    virtual void SetName(const std::string& name) = 0;
    virtual const std::string& GetName() const = 0;
};

}  // namespace mantle_api

#endif  // MANTLEAPI_COMMON_IIDENTIFIABLE_H
