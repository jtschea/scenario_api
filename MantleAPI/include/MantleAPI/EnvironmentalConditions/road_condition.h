/*******************************************************************************
 * Copyright (c) 2021, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

//-----------------------------------------------------------------------------
/** @file  road_condition.h */
//-----------------------------------------------------------------------------

#ifndef MANTLEAPI_ENVIRONMENTALCONDITIONS_ROADCONDITION_H
#define MANTLEAPI_ENVIRONMENTALCONDITIONS_ROADCONDITION_H

#include <MantleAPI/Common/position.h>
#include <units.h>

namespace mantle_api
{

struct Rectangle
{
    Position bottom_left{};
    Position top_right{};
};

struct FrictionPatch
{
    Rectangle bounding_box{};
    units::concentration::percent_t friction{100.0};
};
}  // namespace mantle_api
#endif  // MANTLEAPI_ENVIRONMENTALCONDITIONS_ROADCONDITION_H
