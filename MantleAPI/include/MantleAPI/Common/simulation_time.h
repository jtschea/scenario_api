/*******************************************************************************
 * Copyright (c) 2021, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

//-----------------------------------------------------------------------------
/** @file  simulation_time.h */
//-----------------------------------------------------------------------------

#ifndef MANTLEAPI_COMMON_SIMULATION_TIME_H
#define MANTLEAPI_COMMON_SIMULATION_TIME_H

#include <MantleAPI/Common/time_utils.h>

namespace mantle_api
{

struct SimulationTime
{
    Time current_sim_time{0};
    Time last_delta_time{0};
};

}  // namespace mantle_api
#endif  // MANTLEAPI_COMMON_SIMULATION_TIME_H
